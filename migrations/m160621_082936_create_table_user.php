<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_user`.
 */
class m160621_082936_create_table_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'email' => $this->string(),
            'first_name' => $this->string(),
            'patronymic' => $this->string(),
            'last_name' => $this->string(),
            'city_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'deleted_at' => $this->integer(),
        ], $tableOptions);
        
        $this->createIndex('idx-user-city_id', 'user', 'city_id');

        $this->addForeignKey('fk-user-city_id', 'user', 'city_id', 'city', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-user-city_id', 'user');

        $this->dropIndex('idx-user-city_id', 'user');

        $this->dropTable('user');
    }
}
