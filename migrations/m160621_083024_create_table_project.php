<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_project`.
 */
class m160621_083024_create_table_project extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('project', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->text(),
            'state_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'deleted_at' => $this->integer()
        ], $tableOptions);

        $this->createIndex('idx-project-state_id', 'project', 'state_id');

        $this->addForeignKey('fk-project-state_id', 'project', 'state_id', 'project_state', 'id');
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-project-state_id', 'project');

        $this->dropIndex('idx-project-state_id', 'project');
        
        $this->dropTable('table_project');
    }
}
