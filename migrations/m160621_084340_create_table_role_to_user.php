<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_role_to_user`.
 */
class m160621_084340_create_table_role_to_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('role_to_user', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'project_id' => $this->integer(),
            'role_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'deleted_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-role_to_user-user_id', 'role_to_user', 'user_id');
        $this->createIndex('idx-role_to_user-project_id', 'role_to_user', 'project_id');
        $this->createIndex('idx-role_to_user-role_id', 'role_to_user', 'role_id');

        $this->addForeignKey('fk-role_to_user-user_id', 'role_to_user', 'user_id', 'user', 'id');
        $this->addForeignKey('fk-role_to_user-project_id', 'role_to_user', 'project_id', 'project', 'id');
        $this->addForeignKey('fk-role_to_user-role_id', 'role_to_user', 'role_id', 'role', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-role_to_user-user_id', 'role_to_user');
        $this->dropForeignKey('fk-role_to_user-project_id', 'role_to_user');
        $this->dropForeignKey('fk-role_to_user-role_id', 'role_to_user');

        $this->dropIndex('idx-role_to_user-user_id', 'role_to_user');
        $this->dropIndex('idx-role_to_user-project_id', 'role_to_user');
        $this->dropIndex('idx-role_to_user-role_id', 'role_to_user');

        $this->dropTable('role_to_user');
    }
}
