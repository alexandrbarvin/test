<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProjectState */

$this->title = 'Create Project State';
$this->params['breadcrumbs'][] = ['label' => 'Project States', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-state-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
