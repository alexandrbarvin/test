<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\Project */

use yii\helpers\ArrayHelper;
use app\models\User;
use yii\helpers\Html;

?>
<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <?= Html::label('User') ?>
            <?= Html::activeDropDownList($model,'user_id[]', ArrayHelper::map(User::find()->all(), 'id', 'last_name'), ['prompt' => 'Select User','class'=>'form-control', 'label' => 'User']); ?>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <?= Html::label('Role') ?>
            <?= Html::activeDropDownList($model,'role_id[]', ArrayHelper::map(\app\models\Role::find()->all(), 'id', 'name'), ['prompt' => 'Select role','class'=>'form-control']); ?>
        </div>
    </div>
</div>

