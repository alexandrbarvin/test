<?php

use yii\bootstrap\Html;
use yii\widgets\ActiveForm;
use app\models\ProjectState;
use yii\helpers\ArrayHelper;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="project-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'state_id')->dropDownList(ArrayHelper::map(ProjectState::find()->all(), 'id', 'name'), ['prompt' => 'Select state']); ?>

        <hr/>
        <h3>Добавить пользователей</h3>


        <div id="users">
            <div class="row">
                <?php if (!$model->isNewRecord): ?>
                    <?php foreach ($users as $user): ?>
                        <div class="row">
                            <div class="col-lg-6">
                                <?= $form->field($model, 'user_id[]')->dropDownList(ArrayHelper::map(User::find()->all(),
                                    'id', 'last_name'), [$user['user_id'] => ['Selected' => 'selected']]) ?>
                            </div>
                            <div class="col-lg-6">
                                <?= $form->field($model, 'role_id')->dropDownList(ArrayHelper::map(\app\models\Role::find()->all(),
                                    'id', 'name'), [$user['role_id'] => ['Selected' => 'selected']]) ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::button(Html::icon('plus') . '&nbsp;Add user', ['class' => 'btn btn-primary add-user']) ?>
        </div>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
    <script type="text/template" id="new-user">
        <?= $this->render('_add_user', [
            'model' => $model,
        ]); ?>
    </script>

<?php
$this->registerJs('
        $(document).on(\'click\', \'.add-user\', function(){
            var tpl = $(\'#new-user\').html();
            $(\'#users\').append(tpl);
        });
    ', \yii\web\View::POS_END);
?>