<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Project */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description:ntext',
            [
                'attribute' => 'state_id',
                'label' => 'State',
                'value' => $model->state->name,
            ],
            [
                'attribute' => 'created_at',
                'value' => Yii::$app->formatter->asDatetime($model->created_at),
            ],
            [
                'attribute' => 'updated_at',
                'value' => Yii::$app->formatter->asDatetime($model->updated_at),
            ],
        ],
    ]) ?>

    <?php if (!empty($model->roleToUsers)): ?>
        <h3>Users</h3>
        <table class="table table-striped table-bordered detail-view">
            <?php foreach ($model->roleToUsers as $item): ?>
                <tr>
                    <td>
                        <?= $item->user->last_name ?> <?= $item->user->first_name ?> <?= $item->user->patronymic ?>
                    </td>
                    <td>
                        <?= $item->role->name ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    <?php endif; ?>
</div>
